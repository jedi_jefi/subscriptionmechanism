import Foundation

// MARK: - Given

struct Subscription {
    let identifier: String
}

protocol Generator {
    func generateNumbers(_ onNumberGenerated: @escaping (Int) -> Void)
}

class CustomGenerator: Generator {
    var timer = Timer()
    var closure: ((Int) -> Void)?
    private var counter = 0
    
    func generateNumbers(_ onNumberGenerated: @escaping (Int) -> Void) {
        timer.invalidate()
        self.closure = onNumberGenerated
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
    }
    
    @objc func timerAction() {
        counter += 1
        closure?(counter)
    }
}

class SubscriptionMechanism {
    private var subscriptions = Set<String>()
    private var buffer: Buffer
    private let generator: Generator
    
    init(generator: Generator, bufferSize: Int, transformation: ((Int) -> Int)?) {
        self.generator = generator
        self.buffer = Buffer(bufferSize: bufferSize)
    }
    
    func subscribe(onNextNumber: @escaping (Int) -> Void) -> Subscription {
        let subscription = Subscription(identifier: UUID().uuidString)
        subscriptions.insert(subscription.identifier)
        generator.generateNumbers { [weak self] generatedValue in
            guard let self = self else { return }
            let updatedValue = self.transform(generatedValue)
            if self.subscriptions.contains(subscription.identifier) {
                onNextNumber(updatedValue)
            }
        }
        return subscription
    }
    
    func removeSubscription(_ subscription: Subscription) -> Bool {
        subscriptions.remove(subscription.identifier)
        return true
    }
    
    private func transform(_ generatedValue: Int) -> Int {
        let updatedValue = (0 - generatedValue)
        // Not clear: is buffer replays generated values or the values after transformation?
        self.buffer.add(updatedValue)
        return updatedValue
    }
    
    struct Buffer {
        let bufferSize: Int
        var values = [Int]()
        
        mutating func add(_ newValue: Int) {
            if bufferSize == 0 || values.count < bufferSize {
                values.append(newValue)
            } else {
                values.removeFirst()
            }
        }
    }
}

// MARK: - Usage

let generator = CustomGenerator()
let bufferSize = 8
let mechanism = SubscriptionMechanism(generator: generator, bufferSize: bufferSize, transformation: nil)
let subscription1 = mechanism.subscribe { nextNumber in
    print("Generated value: \(nextNumber)")
}

DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
    _ = mechanism.removeSubscription(subscription1)
    print("Subscription with ID \(subscription1.identifier) is removed")
}
